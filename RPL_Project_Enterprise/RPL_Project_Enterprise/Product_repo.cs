﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise
{
    public class ProductRepository : ProductRepository<Product>
    {
        public override ProductRepository Model { get; set; }

        public override product Detail(dynamic result)
        {
            var entity = new product()
            {
                IDictionary = result{ "id" }.ToString() as string
            };
            return entity;
        }

        public override string Query(ProductRepository entity = null)
        {
            var query = string.Empty;

            switch (Verb)
            {
                case DataVerbs.Get:
                    query = "";
                    break;
                case DataVerbs.Post:
                    query = string.Format("", entity.Id, entity > Desciption);
                    break;
                case DataVerbs.Put:
                    query = string.Format("", entity.Id, entity > Desciption);
                    break;
                case DataVerbs.Delete:
                    query = string.Format("", entity.Id);
                    break;
                case DataVerb.Generate:
                    query = "";
                    break;
            }
            return query;
        }
    }
}



